//Importamos todos los servicios de firebase
/*
1.- firestore
2.- auth
3.- storage
4.- hosting
*/
import firebase from "firebase";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAtVKah-WS0H3TTYOMEohT3jjEpbZ2PkM4",
  authDomain: "medicpill.firebaseapp.com",
  projectId: "medicpill",
  storageBucket: "medicpill.appspot.com",
  messagingSenderId: "444268109580",
  appId: "1:444268109580:web:600d2e18a84f8c39e93329",
  measurementId: "G-6SC5YSBGQB",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
/*
Retornar los servicios de firebase 
*/

const db = firebase.firestore();
const auth = firebase.auth();

/* 
Generamos una librería reutilizable
*/
export default {
  db,
  auth,
};
