import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  contenedor: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titulo: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '500',
    marginVertical: 20,
  },
  textoSidebar: {
    fontSize: 20,
    marginTop :20,
    marginBottom: 5,
  },
  input: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    width: '95%',
    fontSize: 20,
    marginVertical: 10,
  },
  imgLogin: {
    height: 200,
    width: '100%',
    resizeMode: 'contain',
    marginVertical: 30,
  },
  imgIcon: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
  },
  imgRegistro: {
    height: 200,
    width: '100%',
    resizeMode: 'contain',
    marginVertical: 30,
  },
  contenedorImgCircular: {
    width: 200,
    height: 200,
    overflow: 'hidden',
    borderRadius: 100,
  },
  row: {
    flexDirection: 'row',
    width: '95%',
  },
  col: {
    flex: 1,
  },
  col2: {
    flex: 2,
  },
  col3: {
    flex: 3,
  },
});
