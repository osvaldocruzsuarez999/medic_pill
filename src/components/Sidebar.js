import React from "react";
import { ImageBackground, Text, View } from "react-native";
import { Icon } from "native-base";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import { AntDesign } from "@expo/vector-icons";
import estilos from "../styles/estilos";
import { useState } from "react";
import { useEffect } from "react";
import firebase from "../database/firebase";
const Sidebar = (props) => {
  const [userFirebase, setUserFirebase] = useState({});
  const [arrayUser, setArrayUser] = useState({});
  const [DocUser, setDocUser] = useState({});
  const navigation = props.navigation;
  useEffect(() => {
    setUserFirebase(firebase.auth.currentUser);
    getUserlog(firebase.auth.currentUser.uid);
    const navigation = props.navigation;
  }, []);
  const getUserlog = async (uid) => {
    try {
      const query = await firebase.db
        .collection("usuarios")
        .where("authId", "==", uid)
        .get();

      if (!query.empty) {
        const querySnapshot = query.docs;

        const arrayUser = [];
        querySnapshot.forEach((doc) => {
          arrayUser.push({ ...doc.data(), id: doc.id });
        });
        setArrayUser(arrayUser);
        setDocUser(arrayUser[0]);
      }
    } catch (e) {
      console.log(e.toString());
    }
  };

  return (
    <View
      style={{
        flex: 1,
      }}
    >
      <ImageBackground
        source={require("./../../assets/images/fondosidebar.jpg")}
        style={{
          width: "100%",
          paddingBottom: 30,
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <View
            style={{
              flex: 1,
              alignItems: "center",
            }}
          >
            <ImageBackground
              source={require("./../../assets/images/cirujano.png")}
              style={{
                width: 60,
                height: 60,
                overflow: "hidden",
                marginTop: 20,
                borderRadius: 30,
                backgroundColor: "#666",
              }}
            />
          </View>

          <View style={{ flex: 2 }}>
            <View
              style={{
                flex: 1,
                marginLeft: 10,
                marginTop: 20,
              }}
            >
              <Text style={{ ...estilos.textoSidebar, color: "#fff" }}>
                {DocUser.nombres}
              </Text>
            </View>
          </View>
        </View>
      </ImageBackground>

      <DrawerContentScrollView {...props}>
        <DrawerItem
          icon={() => <AntDesign name="home" size={25} color="#000" />}
          label="Inicio"
          onPress={() => {
            props.navigation.navigate("PantInicial");
          }}
        />

        <DrawerItem
          icon={() => <AntDesign name="user" size={25} color="#000" />}
          label="Perfil"
          onPress={() => {
            props.navigation.navigate("Perfil");
          }}
        />

        <DrawerItem
          style={{
            borderTopColor: "#000",
            borderTopWidth: 1,
            paddingTop: 10,
            marginTop: 15,
            marginBottom: 15,
          }}
          icon={() => <AntDesign name="form" size={25} color="#000" />}
          label="Tratamientos"
          onPress={() => {
            props.navigation.navigate("Tratamientos", { user: DocUser });
          }}
        />

        <DrawerItem
          icon={() => <AntDesign name="book" size={25} color="#000" />}
          label="Antecedentes"
          onPress={() => {
            props.navigation.navigate("Antecedentes");
          }}
        />

        <DrawerItem
          icon={() => <Icon name="pulse" size={19} color="#000" />}
          label="Peso"
          onPress={() => {
            props.navigation.navigate("ControlPeso");
          }}
        />

        <DrawerItem
          icon={() => <AntDesign name="phone" size={25} color="#000" />}
          label="Contacto de emergencia"
          onPress={() => {
            props.navigation.navigate("ContactoEmergencia");
          }}
        />
      </DrawerContentScrollView>
    </View>
  );
};

export default Sidebar;
