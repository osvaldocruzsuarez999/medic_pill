import React, { Component } from "react";
import firebase from "./../database/firebase";
import { ActivityIndicator, Alert } from "react-native";
import {
  Container,
  Content,
  Item,
  Input,
  H1,
  Text,
  Thumbnail,
  Button,
  View,
  Label,
} from "native-base";
import estilos from "../styles/estilos";
import get_error from "../helpers/errores_es_mx";
import DatePicker from "react-native-datepicker";

export default class Registro extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nombres: "",
      apellido1: "",
      apellido2: "",
      date: "",
      telefono: "",
      email: "",
      contrasena: "",
      aiVisible: false,
      btnVisible: true,
    };
  }

  render() {
    const validaRegistro = async () => {
      //Valida los nombres
      if (this.state.nombres.length < 3) {
        Alert.alert(
          "ERROR",
          "Nombre incorrecto",
          [
            {
              text: "Corregir",
              onPress: () => this.setState({ nombres: "" }),
            },
          ],
          { cancelable: true }
        );
        return;
      }

      //Valida el apellido paterno

      if (this.state.apellido1.length < 3) {
        Alert.alert(
          "ERROR",
          "Apellido paterno incorrecto",
          [
            {
              text: "Corregir",
              onPress: () => this.setState({ apellido1: "" }),
            },
          ],
          { cancelable: true }
        );
        return;
      }

      //Valida el apellido materno

      if (this.state.apellido2.length > 0 && this.state.apellido2.length < 3) {
        Alert.alert(
          "ERROR",
          "Apellido materno incorrecto",
          [
            {
              text: "Corregir",
              onPress: () => this.setState({ apellido2: "" }),
            },
          ],
          { cancelable: true }
        );
        return;
      }

      //Valida la fecha de nacimiento

      if (this.state.date.length !== 10) {
        let vlfc = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
        if (vlfc.test(this.state.date) == false) {
          Alert.alert(
            "ERROR",
            "La fecha es un campo obligatorio",
            [
              {
                text: "Agregar",
                onPress: () => this.setState({ date: "" }),
              },
            ],
            { cancelable: true }
          );
          return;
        }
      }

      //Valida el número de teléfono

      if (this.state.telefono.length !== 10) {
        Alert.alert(
          "ERROR",
          "Teléfono incorrecto",
          [
            {
              text: "Corregir",
              onPress: () => this.setState({ telefono: "" }),
            },
          ],
          { cancelable: true }
        );
        return;
      }

      //Valida la contraseña
      if (this.state.contrasena.length < 3) {
        Alert.alert(
          "ERROR",
          "La contraseña debe ser mayor a 3 caracteres",
          [
            {
              text: "Corregir",
              onPress: () => this.setState({ contrasena: "" }),
            },
          ],
          { cancelable: true }
        );
        return;
      }

      //Si las validaciones son correctas
      this.setState({
        aiVisible: true,
        btnVisible: false,
      });

      /* Crear un documento en la colección de usuarios */
      try {
        /**
         *Se crea un usuario desde el servicio de auth de Firebase
         */
        const usuarioFirebase = await firebase.auth.createUserWithEmailAndPassword(
          this.state.email,
          this.state.contrasena
        );

        /**
         * Se envia un correo electronico para validar la cuenta
         */

        await usuarioFirebase.user.sendEmailVerification().then(() => {
          Alert.alert(
            "USUARIO REGISTRADO",
            `${usuarioFirebase.user.uid}\n Se ha enviado un correo para que valides tu cuenta`,
            [
              {
                text: "Continuar",
                onPress: () => {
                  this.setState({
                    aiVisible: false,
                    btnVisible: true,
                  });
                },
              },
            ]
          );
        });

        const docUsuario = await firebase.db.collection("usuarios").add({
          authId: usuarioFirebase.user.uid,
          nombres: this.state.nombres,
          apellido1: this.state.apellido1,
          apellido2: this.state.apellido2,
          date: this.state.date,
          telefono: this.state.telefono,
        });
      } catch (e) {
        console.log(e.toString());
        Alert.alert("ERROR", get_error(e.code), [
          {
            text: "Corregir",
            onPress: () => {
              this.setState({
                aiVisible: false,
                btnVisible: true,
              });
            },
          },
        ]);
      }
    };

    return (
      //vista del app
      <Container>
        <Content padder>
          <Thumbnail
            source={require("./../../assets/images/registro.png")}
            style={estilos.imgRegistro}
          />
          <H1
            style={{ textAlign: "center", color: "#1d3557", marginBottom: 20 }}
          >
            Registro
          </H1>

          <Text style={{ size: 10, marginBottom: 20 }}>
            * Campos requeridos{" "}
          </Text>

          <Item floatingLabel style={{ marginBottom: 20 }}>
            <Label>Nombre(s)*</Label>
            <Input
              keyboardType='default'
              maxLength={60}
              style={estilos.input}
              value={this.state.nombres}
              onChangeText={(val) => {
                this.setState({ nombres: val });
              }}
            />
          </Item>
          <View style={estilos.row}>
            <View style={estilos.col}>
              <Item floatingLabel style={{ marginBottom: 20 }}>
                <Label>Apellido paterno*</Label>
                <Input
                  keyboardType='default'
                  maxLength={23}
                  style={estilos.input}
                  value={this.state.apellido1}
                  onChangeText={(val) => {
                    this.setState({ apellido1: val });
                  }}
                />
              </Item>
            </View>
            <View style={estilos.col}>
              <Item floatingLabel style={{ marginBottom: 20 }}>
                <Label>Apellido materno</Label>
                <Input
                  keyboardType='default'
                  maxLength={23}
                  style={estilos.input}
                  value={this.state.apellido2}
                  onChangeText={(val) => {
                    this.setState({ apellido2: val });
                  }}
                />
              </Item>
            </View>
          </View>

          <View style={estilos.row}>
            <View style={estilos.col}>
              <Item>
                <DatePicker
                  style={estilos.input}
                  date={this.state.date}
                  mode='date'
                  placeholder='Fecha de nacimiento*'
                  format='YYYY-MM-DD'
                  minDate='1950-01-01'
                  maxDate='2021-12-31'
                  confirmBtnText='Confirmar'
                  cancelBtnText='Cancelar'
                  customStyles={{
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 4,
                      marginLeft: 0,
                    },
                    dateInput: {
                      marginLeft: 30,
                      borderColor: "#fff",
                    },
                  }}
                  onDateChange={(date) => {
                    this.setState({ date: date });
                  }}
                />
              </Item>
            </View>
            <View style={estilos.col}>
              <Item floatingLabel style={{ marginBottom: 20 }}>
                <Label>Teléfono (10 dígitos)*</Label>
                <Input
                  keyboardType='phone-pad'
                  maxLength={10}
                  style={estilos.input}
                  value={this.state.telefono}
                  onChangeText={(val) => {
                    this.setState({ telefono: val });
                  }}
                />
              </Item>
            </View>
          </View>
          <Item floatingLabel style={{ marginBottom: 20 }}>
            <Label>Email*</Label>
            <Input
              keyboardType='email-address'
              maxLength={55}
              style={estilos.input}
              value={this.state.email}
              onChangeText={(val) => {
                this.setState({ email: val });
              }}
              autoCapitalize='none'
            />
          </Item>

          <Item floatingLabel style={{ marginBottom: 20 }}>
            <Label>Contraseña*</Label>
            <Input
              keyboardType='default'
              secureTextEntry
              maxLength={25}
              style={estilos.input}
              value={this.state.contrasena}
              onChangeText={(val) => {
                this.setState({ contrasena: val });
              }}
            />
          </Item>

          <ActivityIndicator
            color='#1d3557'
            size='large'
            style={{ display: this.state.aiVisible ? "flex" : "none" }}
          />

          <View
            style={{
              margin: 15,
              display: this.state.btnVisible ? "flex" : "none",
              alignSelf: "center",
            }}
          >
            <Button
              style={{ backgroundColor: "#457b9d" }}
              onPress={validaRegistro}
            >
              <Text>Registrarse</Text>
            </Button>
          </View>
          <Button
            style={{ alignSelf: "center" }}
            transparent
            onPress={() => {
              this.props.navigation.navigate("Login");
            }}
          >
            <Text style={{ color: "#457b9d" }}>
              ¿Ya tienes una cuenta?, inicia sesión aquí
            </Text>
          </Button>
        </Content>
      </Container>
    );
  }
}
