import { useFocusEffect } from "@react-navigation/core";
import React from "react";
import {
  Container,
  Button,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text,
  View,
} from "native-base";
import estilos from "../../styles/estilos";
import { ActivityIndicator, Alert } from "react-native";
import { useState, useEffect } from "react";
import firebase from "../../database/firebase";
const EditarTratamiento = (props) => {
  let data = props.route.params.ArrayTrat;
  let user = props.route.params.userid;
  console.log("data", data);
  console.log("user", user);
  const [Cargando, setCargando] = useState(false);
  const [userFirebase, setUserFirebase] = useState({});
  const [arrayUser, setArrayUser] = useState({});
  const [DocUser, setDocUser] = useState({});
  const [datas, setData] = useState([]);
  useEffect(() => {
    getTrat(data, user);
  }, []);
  const getTrat = async (data, user) => {
    try {
      const query = await firebase.db
        .collection("usuarios")
        .doc(user)
        .collection("tratamientos")
        .doc(data)
        .onSnapshot((querySnapshot) => {
          try {
            const datas = [];
            querySnapshot.docs.forEach((doc) => {
              const { dosis, duracion, fecha, nombre } = doc.datas();
              datas.push({
                id: doc.id,
                dosis,
                duracion,
                fecha,
                nombre,
              });
            });
            setData(datas);
          } catch (e) {
            console.log(e);
          }
        });
    } catch (e) {
      console.warn(e.toString());
    }
  };

  useFocusEffect(() => {
    props.navigation.dangerouslyGetParent().setOptions({
      title: "Editar Tratameinto",
    });
  });

  return (
    <Container>
      <Content>
        <Form>
          <Item floatingLabel style={{ marginTop: 20 }}>
            <Label>Nombre medicamento</Label>
            <Input
              value={data.nombre}
              onChangeText={(value) => setVal({ ...Val, ["nombre"]: value })}
            />
          </Item>

          <Item floatingLabel last>
            <Label>Cantidad / Dosis</Label>
            <Input
              value={data.dosis}
              onChangeText={(value) => setVal({ ...Val, ["dosis"]: value })}
            />
          </Item>

          <Item floatingLabel last>
            <Label>Duración</Label>
            <Input
              value={data.duracion}
              onChangeText={(value) => setVal({ ...Val, ["duracion"]: value })}
            />
          </Item>
          <View>
            <ActivityIndicator
              animating={Cargando}
              size="large"
              color="#52b69a"
            />
          </View>
          <View style={estilos.row} marginTop={50}>
            <View style={estilos.col}>
              <Button
                style={{ alignSelf: "center", backgroundColor: "#a4161a" }}
                onPress={() => {
                  props.navigation.navigate("Tratamientos");
                }}
              >
                <Text> Cancelar </Text>
              </Button>
            </View>
            <View style={estilos.col}>
              <Button
                style={{ alignSelf: "center", backgroundColor: "#52b69a" }}
              >
                <Text> Agregar </Text>
              </Button>
            </View>
          </View>
        </Form>
      </Content>
    </Container>
  );
};

export default EditarTratamiento;
