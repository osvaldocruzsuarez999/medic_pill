import { useFocusEffect } from "@react-navigation/core";
import React from "react";
import {
  Container,
  Button,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text,
  View,
} from "native-base";
import { Alert } from "react-native";
import estilos from "../../styles/estilos";
const ControlPesoAgregar = (props) => {
  /*
    Efecto qu actualice el título del Stack Navigator  
    desde los componentes Drawer Hijos
    Esto se tiene que actualizar cada vez aue cambiamos de Drawer.Screnn
     */
  useFocusEffect(() => {
    //Modificamos las opciones del HEader del Stack (padre)
    props.navigation.dangerouslyGetParent().setOptions({
      title: "Agregar Peso",
    });
  });

  const validaDatos = () =>
    Alert.alert(
      "TU PESO HA SIDO AGREGADO",
      "se agregara el peso a la bd",
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: false }
    );

  return (
    <Container>
      <Content>
        <Form>
          <Item floatingLabel style={{ marginTop: 20 }}>
            <Label>Fecha (dd/mm/aaaa)</Label>
            <Input />
          </Item>

          <Item floatingLabel last>
            <Label>Hora</Label>
            <Input />
          </Item>

          <Item floatingLabel last>
            <Label>Peso</Label>
            <Input />
          </Item>

          <Item floatingLabel last>
            <Label>Estatura</Label>
            <Input />
          </Item>

          <View style={estilos.row} marginTop={50}>
            <View style={estilos.col}>
              <Button
                style={{ alignSelf: "center", backgroundColor: "#a4161a" }}
                onPress={() => {
                  props.navigation.navigate("ControlPeso");
                }}
              >
                <Text> Cancelar </Text>
              </Button>
            </View>
            <View style={estilos.col}>
              <Button
                style={{ alignSelf: "center", backgroundColor: "#52b69a" }}
                onPress={validaDatos}
              >
                <Text> Agregar </Text>
              </Button>
            </View>
          </View>
        </Form>
      </Content>
    </Container>
  );
};

export default ControlPesoAgregar;
