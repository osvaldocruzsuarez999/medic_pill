import { useFocusEffect } from "@react-navigation/core";
import React from "react";
import {
  Container,
  Button,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text,
  Textarea,
  View,
} from "native-base";

import { Alert } from "react-native";
import estilos from "../../styles/estilos";

const AntecedenteAgregar = (props) => {
  useFocusEffect(() => {
    //Modificamos las opciones del HEader del Stack (padre)
    props.navigation.dangerouslyGetParent().setOptions({
      title: "Agregar antecedente",
    });
  });

  const validaDatos = () =>
    Alert.alert(
      "AANTECEDENTE AGREGADO",
      "se agregara el antecedente a la bd",
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: false }
    );

  return (
    <Container>
      <Content>
        <Form margin={10}>
          <Item floatingLabel style={{ marginTop: 20 }}>
            <Label>Alergia(s)</Label>
            <Input
              keyboardType='default'
              maxLength={60}
              style={estilos.input}
            />
          </Item>

          <Item floatingLabel>
            <Label>Procedimiento(s)</Label>
            <Input />
          </Item>

          <Item floatingLabel>
            <Label>Enfermedades</Label>
            <Input
              keyboardType='default'
              maxLength={60}
              style={estilos.input}
            />
          </Item>

          <View style={estilos.row} marginTop={50}>
            <View style={estilos.col}></View>
            <View style={estilos.col}></View>
          </View>

          <View style={estilos.row} marginTop={50}>
            <View style={estilos.col}>
              <Button
                style={{ alignSelf: "center", backgroundColor: "#a4161a" }}
                onPress={() => {
                  props.navigation.navigate("Antecedentes");
                }}
              >
                <Text> Cancelar </Text>
              </Button>
            </View>
            <View style={estilos.col}>
              <Button
                style={{ alignSelf: "center", backgroundColor: "#52b69a" }}
                onPress={validaDatos}
              >
                <Text> Agregar </Text>
              </Button>
            </View>
          </View>
        </Form>
      </Content>
    </Container>
  );
};

export default AntecedenteAgregar;
