import { useFocusEffect } from '@react-navigation/core';
import React from 'react';
import {
  Container,
  Content,
  View,
  ListItem,
  Text,
  Fab,
  Icon,
} from 'native-base';
import { Entypo } from '@expo/vector-icons';
import estilos from '../../styles/estilos';
const ControlPeso = (props) => {
  useFocusEffect(() => {
    //Modificamos las opciones del HEader del Stack (padre)
    props.navigation.dangerouslyGetParent().setOptions({
      title: 'Control Peso',
    });
  });

  return (
    <Container>
      <Content>
        <ListItem>
          <Text>Peso 1</Text>
        </ListItem>
        <ListItem last>
          <Text>Peso 2</Text>
        </ListItem>
        <ListItem>
          <Text>Peso 3</Text>
        </ListItem>
        <ListItem last>
          <Text>Peso 4</Text>
        </ListItem>
        <ListItem>
          <Text>Peso 5</Text>
        </ListItem>
      </Content>

      <Fab
        containerStyle={{}}
        style={{ backgroundColor: '#52b69a' }}
        position='bottomRight'
        onPress={() => {
          props.navigation.navigate('ControlPesoAgregar');
        }}
      >
        <Entypo name='plus' />
      </Fab>
    </Container>
  );
};

export default ControlPeso;
