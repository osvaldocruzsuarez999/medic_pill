import { useFocusEffect } from "@react-navigation/core";
import React, { useEffect, useState } from "react";
import { ImageBackground, SafeAreaView, TouchableOpacity } from "react-native";
import {
  Container,
  Content,
  Item,
  Input,
  Text,
  Button,
  View,
  Label,
} from "native-base";
import DatePicker from "react-native-datepicker";
import { FontAwesome5 } from "@expo/vector-icons";
import firebase from "../../database/firebase";
import estilos from "../../styles/estilos";
import ProgressDialog from "./../../components/ProgressDialog";
import Snack from "react-native-snackbar-component";
import AppModal from "./../../components/AppModal";
import * as ImagePicker from "expo-image-picker";

const Perfil = (props) => {
  const [usuarioFirebase, setUsuarioFirebase] = useState({});
  const [docUsuario, setDocUsuario] = useState({});
  const [loading, setLoading] = useState(true);
  const [snackUpdate, setSnackUpdate] = useState(false);
  const [snackError, setSnackError] = useState(false);
  const [modalImg, setModalImg] = useState(false);

  /**
   * Creamos un hook que nos permita conectar al cargarse el screen
   * con los datos de dicho usuario desde la colección usuaio
   *
   * No se debe convertir un efectoen una fn asincrona, si es necesario
   * usar un hook y una promesa, se debe crear una función dentro del hook
   * o bien crear una funcion flecha e invocarla en el hook
   */
  useEffect(() => {
    /* tomamos los datos del usuario que ha iniciado sesión */
    setUsuarioFirebase(firebase.auth.currentUser);

    /* invocamos la consulta */
    getDocUsuario(firebase.auth.currentUser.uid);
  }, []);

  /**
   * Función flecha que ejecuta una consulta sobre la colección
   * usuarios
   */
  const getDocUsuario = async (uid) => {
    try {
      const query = await firebase.db
        .collection("usuarios")
        /**
         * Where usa 3 parámetros
         * 1.- Clave a comparar (campo en la tabla)
         * 2.- Tipo de condición (leer documentación)
         * 3.- Valor de la condición
         */
        .where("authId", "==", uid)
        .get();

      /**
       * Si la consulta no esta vacía
       */
      if (!query.empty) {
        /**
         * query contiene un snapshot llamado docs
         * y es una arreglo con todos los documentos
         * de la consulta
         */
        /* cuando esperamos varios registros en una consulta recorremos a doc */
        // query.docs.forEach((doc) => {
        // 	console.log(doc.data());
        // });

        /* cuando esperamos solo un registro */
        const snapshot = query.docs[0];
        setDocUsuario({
          ...snapshot.data(),
          id: snapshot.id,
        });

        setLoading(false);
      }
    } catch (e) {
      console.warn(e.toString());
    }
  };

  useFocusEffect(() => {
    props.navigation.dangerouslyGetParent().setOptions({
      title: "Perfil",
    });
  });

  /**
   * Creamos una constante para tomar una imagen desde
   * la galería
   * (Image Gallery - Android)
   * (Camera Roll - iOS)
   *
   * 0.- Importar librería ImagePicker y todos sus componentes
   * 1.- Pedir permiso para acceder a la mutimedia
   * 2.- Indicar el tipo de multimedia que necesitamos (photo/video/all)
   * 3.- Indicar si se podrá editar la imagen
   * 4.- Indicar la relación de aspecto
   */
  const getImagenGaleria = async () => {
    /**
     * Preguntamos por el permiso para acceder a
     * los elementos multimedia de la galería
     */
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();

    /**
     * Si el usuario nos da permiso de ingresar a su galería
     * Mostramos todas sus fotos y esperamos que seleccione una
     */
    if (status === "granted") {
      const imgGaleria = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 4],
        quality: 1,
      });

      console.log(imgGaleria);
    }
  };

  return (
    /**
     * SafeAreaView calcula el espacio donde el texto
     * no se visualiza y lo recorre
     */ <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <Snack
          textMessage='Datos actualizados'
          messageColor='#fff'
          backgroundColor='#376e37'
          actionText='Entendido'
          accentColor='#5cb85c'
          actionHandler={() => {
            setSnackUpdate(false);
          }}
          visible={snackUpdate}
        />

        <Snack
          textMessage='Ocurrió un error'
          messageColor='#fff'
          backgroundColor='red'
          actionText='Entendido'
          accentColor='#fff'
          actionHandler={() => setSnackError(false)}
          visible={snackError}
        />

        {modalImg ? (
          <AppModal
            show={modalImg}
            layerBgColor='#333'
            layerBgOpacity={0.5}
            modalBgColor='#fff'
            modalOpacity={1}
            modalContent={
              <View>
                <Text
                  style={{
                    alignSelf: "center",
                    marginBottom: 20,
                    fontSize: 20,
                    fontWeight: "500",
                  }}
                >
                  <FontAwesome5 name='camera-retro' size={20} /> Actualizar foto
                  de perfíl
                </Text>

                <Button block transparent light>
                  <Text>Tomar foto</Text>
                </Button>
                {Platform.OS === "android" ? (
                  <View
                    style={{
                      marginVertical: 7,
                    }}
                  />
                ) : null}

                <Button block transparent light onPress={getImagenGaleria}>
                  <Text>Galería</Text>
                </Button>

                {Platform.OS === "android" ? (
                  <View
                    style={{
                      marginVertical: 7,
                    }}
                  />
                ) : null}

                <Button block danger onPress={() => setModalImg(false)}>
                  <Text>Cancelar</Text>
                </Button>
              </View>
            }
          />
        ) : null}

        <Content padder>
          {loading ? <ProgressDialog /> : null}

          <TouchableOpacity onPress={() => setModalImg(true)}>
            <ImageBackground
              source={require("./../../../assets/images/perdonar.jpg")}
              style={{
                width: 200,
                height: 200,
                alignSelf: "center",
                marginVertical: 15,
                marginBottom: 30,
                marginTop: 24,
                borderRadius: 25,
                overflow: "hidden",
              }}
            >
              <Text
                style={{
                  backgroundColor: "#000",
                  color: "#fff",
                  width: "100%",
                  paddingBottom: 20,
                  paddingTop: 10,
                  textAlign: "center",
                  opacity: 0.6,
                  position: "absolute",
                  bottom: 0,
                }}
              >
                <FontAwesome5 name='camera' color='#fff' /> Cambiar imagen
              </Text>
            </ImageBackground>
          </TouchableOpacity>

          <Item floatingLabel style={{ marginBottom: 20 }}>
            <Label>Nombre(s)*</Label>
            <Input
              keyboardType='default'
              maxLength={60}
              style={estilos.input}
              value={docUsuario.nombres}
              onChangeText={(val) =>
                setDocUsuario({
                  ...docUsuario,
                  ["nombres"]: val,
                })
              }
            />
          </Item>

          <View style={estilos.row}>
            <View style={estilos.col}>
              <Item floatingLabel style={{ marginBottom: 20 }}>
                <Label>Apellido paterno*</Label>
                <Input
                  keyboardType='default'
                  maxLength={23}
                  style={estilos.input}
                  value={docUsuario.apellido1}
                  onChangeText={(val) =>
                    setDocUsuario({
                      ...docUsuario,
                      ["apellido1"]: val,
                    })
                  }
                />
              </Item>
            </View>
            <View style={estilos.col}>
              <Item floatingLabel style={{ marginBottom: 20 }}>
                <Label>Apellido materno</Label>
                <Input
                  keyboardType='default'
                  maxLength={23}
                  style={estilos.input}
                  value={docUsuario.apellido2}
                  onChangeText={(val) =>
                    setDocUsuario({
                      ...docUsuario,
                      ["apellido2"]: val,
                    })
                  }
                />
              </Item>
            </View>
          </View>

          <View style={estilos.row}>
            <View style={estilos.col}>
              <Item>
                <DatePicker
                  style={estilos.input}
                  date={docUsuario.date}
                  mode='date'
                  placeholder='Fecha de nacimiento*'
                  format='YYYY-MM-DD'
                  minDate='1950-01-01'
                  maxDate='2021-12-31'
                  confirmBtnText='Confirmar'
                  cancelBtnText='Cancelar'
                  customStyles={{
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 4,
                      marginLeft: 0,
                    },
                    dateInput: {
                      marginLeft: 30,
                      borderColor: "#fff",
                    },
                  }}
                  onChangeText={(val) =>
                    setDocUsuario({
                      ...docUsuario,
                      ["date"]: val,
                    })
                  }
                />
              </Item>
            </View>
            <View style={estilos.col}>
              <Item floatingLabel style={{ marginBottom: 20 }}>
                <Label>Teléfono (10 dígitos)*</Label>
                <Input
                  keyboardType='phone-pad'
                  maxLength={10}
                  style={estilos.input}
                  value={docUsuario.telefono}
                  onChangeText={(val) =>
                    setDocUsuario({
                      ...docUsuario,
                      ["telefono"]: val,
                    })
                  }
                />
              </Item>
            </View>
          </View>

          <Item floatingLabel style={{ marginBottom: 20 }}>
            <Label>Email*</Label>
            <Input
              keyboardType='email-address'
              maxLength={55}
              style={estilos.input}
              value={usuarioFirebase.email}
              autoCapitalize='none'
              editable={false}
            />
          </Item>

          <View style={estilos.row} marginTop={30}>
            <View style={estilos.col}>
              <Button
                style={{ alignSelf: "center", backgroundColor: "#52b69a" }}
                onPress={async () => {
                  setLoading(true);

                  /**
                   * Existen dos tipos de edicion de datos en
                   * FireStore
                   *
                   * 1.- update (constructivo)
                   *      Solo se editan los campos indicados
                   *      y los demás se respetan
                   * 2.- set (destructivo)
                   *      Solo se editan los campos indicados
                   *      y los demás se eliminan
                   */
                  try {
                    //Seleccionamos de toda la coleccion
                    //solo el elemento del id de ese
                    //documento
                    await firebase.db
                      .collection("usuarios")
                      .doc(docUsuario.id)
                      .update({
                        nombres: docUsuario.nombres,
                        apellido1: docUsuario.apellido1,
                        apellido2: docUsuario.apellido2,
                        date: docUsuario.date,
                        telefono: docUsuario.telefono,
                      });
                    setLoading(false);
                    setSnackUpdate(true);
                  } catch (e) {
                    setLoading(false);
                    setSnackError(true);
                  }
                }}
              >
                <Text> Guardar </Text>
              </Button>
            </View>
          </View>
        </Content>
      </Container>
    </SafeAreaView>
  );
};

export default Perfil;
