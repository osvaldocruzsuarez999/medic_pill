import { useFocusEffect } from "@react-navigation/core";
import React from "react";
import {
  Container,
  Button,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text,
  View,
} from "native-base";
import estilos from "../../styles/estilos";
import { ActivityIndicator, Alert } from "react-native";
import { useState, useEffect } from "react";
import firebase from "../../database/firebase";
const TratamientoAgregar = (props) => {
  const [userFirebase, setUserFirebase] = useState({});
  const [arrayUser, setArrayUser] = useState({});
  const [DocUser, setDocUser] = useState({});

  useEffect(() => {
    setUserFirebase(firebase.auth.currentUser);
    getUserlog(firebase.auth.currentUser.uid);
  }, []);
  const getUserlog = async (uid) => {
    try {
      const query = await firebase.db
        .collection("usuarios")
        .where("authId", "==", uid)
        .get();

      if (!query.empty) {
        const querySnapshot = query.docs;

        const arrayUser = [];
        querySnapshot.forEach((doc) => {
          arrayUser.push({ ...doc.data(), id: doc.id });
        });
        setArrayUser(arrayUser);
        setDocUser(arrayUser[0]);
      }
    } catch (e) {
      console.log(e.toString());
    }
  };

  useFocusEffect(() => {
    props.navigation.dangerouslyGetParent().setOptions({
      title: "Agregar Tratameinto",
    });
  });
  const [Cargando, setCargando] = useState(false);
  const [state, setState] = useState({
    nombre: "",
    cantidad: "",
    duracion: "",
    status: 1,
  });
  const handleChangeText = (name, value) => {
    setState({ ...state, [name]: value });
  };
  const validaDatos = async () => {
    if ((state.nombre == "" && state.cantidad == "") || state.duracion == "") {
      Alert.alert(
        "ERROR EN FORMULARIO",
        "Faltan datos en el formulario",
        [
          {
            text: "OK",
            onPress: () => null,
          },
        ],
        { cancelable: false }
      );
    } else {
      setCargando(true);
      firebase.db
        .collection("usuarios")
        .doc(DocUser.id)
        .collection("tratamientos")
        .add({
          dosis: state.cantidad,
          duracion: state.duracion,
          fecha: Date(),
          nombre: state.nombre,
        });
      Alert.alert(
        "TRATAMIENTO AGREGADO",
        "El tratamiento fue agregado con exito",
        [
          {
            text: "OK",
            onPress: () => null,
          },
        ],
        { cancelable: false }
      );

      setCargando(false);

      try {
      } catch (e) {}
    }
  };

  return (
    <Container>
      <Content>
        <Form>
          <Item floatingLabel style={{ marginTop: 20 }}>
            <Label>Nombre medicamento</Label>
            <Input
              onChangeText={(value) => handleChangeText("nombre", value)}
            />
          </Item>

          <Item floatingLabel last>
            <Label>Cantidad / Dosis</Label>
            <Input
              onChangeText={(value) => handleChangeText("cantidad", value)}
            />
          </Item>

          <Item floatingLabel last>
            <Label>Duración</Label>
            <Input
              onChangeText={(value) => handleChangeText("duracion", value)}
            />
          </Item>
          <View>
            <ActivityIndicator
              animating={Cargando}
              size="large"
              color="#52b69a"
            />
          </View>
          <View style={estilos.row} marginTop={50}>
            <View style={estilos.col}>
              <Button
                style={{ alignSelf: "center", backgroundColor: "#a4161a" }}
                onPress={() => {
                  props.navigation.navigate("Tratamientos");
                }}
              >
                <Text> Cancelar </Text>
              </Button>
            </View>
            <View style={estilos.col}>
              <Button
                style={{ alignSelf: "center", backgroundColor: "#52b69a" }}
                onPress={validaDatos}
              >
                <Text> Agregar </Text>
              </Button>
            </View>
          </View>
        </Form>
      </Content>
    </Container>
  );
};

export default TratamientoAgregar;
