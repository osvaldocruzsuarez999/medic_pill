import { useFocusEffect } from '@react-navigation/core';
import React from 'react';
import {
  Container,
  Content,
  View,
  ListItem,
  Text,
  Fab,
  Icon,
} from 'native-base';
import { Entypo } from '@expo/vector-icons';

const Antecedentes = (props) => {
  useFocusEffect(() => {
    //Modificamos las opciones del HEader del Stack (padre)
    props.navigation.dangerouslyGetParent().setOptions({
      title: 'Antecedentes',
    });
  });
  return (
    <Container>
      <Content>
        <ListItem>
          <Text>Antecedente 1</Text>
        </ListItem>
        <ListItem last>
          <Text>Antecedente 2</Text>
        </ListItem>
        <ListItem>
          <Text>Antecedente 3</Text>
        </ListItem>
        <ListItem last>
          <Text>Antecedente 4</Text>
        </ListItem>
        <ListItem>
          <Text>Antecedente 5</Text>
        </ListItem>
        <ListItem last>
          <Text>Antecedente 6</Text>
        </ListItem>
      </Content>

      <Fab
        containerStyle={{}}
        style={{ backgroundColor: '#52b69a' }}
        position='bottomRight'
        onPress={() => {
          props.navigation.navigate('AntecedenteAgregar');
        }}
      >
        <Entypo name='plus' />
      </Fab>
    </Container>
  );
};

export default Antecedentes;
