import { useFocusEffect } from "@react-navigation/core";
import React, { useEffect, useState } from "react";
import { SafeAreaView, View } from "react-native";
import Snackbar from "react-native-snackbar-component";
import firebase from "./../../database/firebase";
import { Container, Content, Text, Icon } from "native-base";
import { Entypo } from "@expo/vector-icons";

const PantallaInicial = (props) => {
  const [snack, setSnack] = useState(false);

  useFocusEffect(() => {
    //Modificamos las opciones del HEader del Stack (padre)
    props.navigation.dangerouslyGetParent().setOptions({
      title: "Inicio",
    });
  });

  /* Creamos un efecto (solo se ejecute al inicio)
    que nos permite ir por los datos del usuario 
    que ha iniciado sesión */
  useEffect(() => {
    /*
        Verificamos si tenemos info de algún usuario de manera
        local
        */
    const usuarioFirebase = firebase.auth.currentUser;

    /*
        Revisamos si el usuario no está validado
        */
    if (!usuarioFirebase.emailVerified) {
      setSnack(true);
    }
  }, []);

  return (
    <Container>
      <Content>
        <Text>Patalla inicial </Text>
      </Content>
      <SafeAreaView style={{ flex: 1 }}>
        <Snackbar
          visible={snack}
          textMessage='Hola Snack'
          backgroundColor='#dc3545'
          textMessage='Tu cuenta aun no ha sido verificada'
          actionText='Ok'
          actionHandler={() => {
            setSnack(false);
          }}
        />
      </SafeAreaView>
    </Container>
  );
};

export default PantallaInicial;
