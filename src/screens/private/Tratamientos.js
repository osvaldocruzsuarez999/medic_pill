import { useFocusEffect, useNavigation } from "@react-navigation/core";
import React, { useEffect } from "react";

import {
  Container,
  Content,
  ListItem,
  Text,
  Fab,
  Card,
  CardItem,
  Left,
  Body,
  Right,
  Button,
} from "native-base";
import { Entypo } from "@expo/vector-icons";
import { useState } from "react/cjs/react.development";
import firebase from "../../database/firebase";
import { Alert } from "react-native";
const Tratamientos = (props) => {
  const navigation = props.navigation;
  let user = props.route.params.user;

  const [userFirebase, setUserFirebase] = useState({});
  const [arrayUser, setArrayUser] = useState({});
  const [DocUser, setDocUser] = useState({});
  const [data, setData] = useState([]);
  const [idUser, setIdUser] = useState("");
  const [uid, setUid] = useState(user.authId);
  useEffect(() => {
    getDatos();
  }, []);

  const getDatos = async () => {
    await firebase.db
      .collection("usuarios")
      .doc(user.id)
      .collection("tratamientos")
      .where("status", "==", 1)
      .onSnapshot((querySnapshot) => {
        try {
          const data = [];
          querySnapshot.docs.forEach((doc) => {
            const { dosis, duracion, fecha, nombre } = doc.data();
            data.push({
              id: doc.id,
              dosis,
              duracion,
              fecha,
              nombre,
            });
          });
          setData(data);
        } catch (e) {
          console.log(e);
        }
      });
  };
  const sendEditTratamiento = (id, userid) => {
    props.navigation.navigate("EditarTratamiento", {
      ArrayTrat: id,
      userid: userid,
    });
  };
  const DeleteThis = async (id) => {
    Alert.alert(
      "Eliminar tratamiento",
      "esta seguro de eliminar el tratamiento",
      [
        {
          text: "Cancelar ",
          onPress: () => null,
        },
        {
          text: "Ok",
          onPress: () =>
            firebase.db
              .collection("usuarios")
              .doc(user.id)
              .collection("tratamientos")
              .doc(id)
              .update({ status: 0 }),
        },
      ],
      { cancelable: false }
    );
  };
  return (
    <Container>
      <Content>
        {data.map((d) => (
          <Card key={d.id}>
            <CardItem>
              <Content>
                <Text>
                  {" "}
                  {d.nombre}
                  {"\n"}dosis:{d.dosis} {"\n"}duracion:{d.duracion}
                  {"\n"}fecha:{d.fecha}{" "}
                </Text>
              </Content>
              <Content style={{ margin: 10 }}>
                <Button
                  block
                  rounded
                  info
                  onPress={() => sendEditTratamiento(d.id, user.id)}
                >
                  <Text>Editar</Text>
                </Button>
              </Content>
              <Content style={{ margin: 10 }}>
                <Button block rounded danger onPress={() => DeleteThis(d.id)}>
                  <Text>Eliminar</Text>
                </Button>
              </Content>
            </CardItem>
          </Card>
        ))}
      </Content>

      <Fab
        containerStyle={{}}
        style={{ backgroundColor: "#52b69a" }}
        position="bottomRight"
        onPress={() => {
          props.navigation.navigate("TratamientoAgregar");
        }}
      >
        <Entypo name="plus" />
      </Fab>
    </Container>
  );
};

export default Tratamientos;
