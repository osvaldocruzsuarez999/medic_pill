import { useFocusEffect } from "@react-navigation/core";
import React from "react";
import {
  Container,
  Button,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text,
  Textarea,
  View,
} from "native-base";
import estilos from "../../styles/estilos";
import { Alert } from "react-native";
const ContactoEmergenciaAgregar = (props) => {
  /*
    Efecto qu actualice el título del Stack Navigator  
    desde los componentes Drawer Hijos
    Esto se tiene que actualizar cada vez aue cambiamos de Drawer.Screnn
     */
  useFocusEffect(() => {
    //Modificamos las opciones del HEader del Stack (padre)
    props.navigation.dangerouslyGetParent().setOptions({
      title: "Agregar contacto de emergencia",
    });
  });

  const validaDatos = () =>
    Alert.alert(
      "CONTACTO DE EMERGENCIA AGREGADO",
      "se agregara el contacto de emergencia a la bd",
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: false }
    );

  return (
    <Container>
      <Content>
        <Form margin={10}>
          <Item floatingLabel style={{ marginTop: 20 }}>
            <Label>Nombre(s)</Label>
            <Input
              keyboardType='default'
              maxLength={60}
              style={estilos.input}
            />
          </Item>

          <Item floatingLabel>
            <Label>Apellido(s)</Label>
            <Input />
          </Item>

          <Item floatingLabel>
            <Label>Parentesco</Label>
            <Input
              keyboardType='default'
              maxLength={60}
              style={estilos.input}
            />
          </Item>

          <Item floatingLabel>
            <Label>Edad</Label>
            <Input
              keyboardType='default'
              maxLength={60}
              style={estilos.input}
            />
          </Item>

          <Item>
            <Textarea
              rowSpan={3}
              marginTop={20}
              style={estilos.input}
              placeholder='Dirección'
            />
          </Item>

          <View style={estilos.row} marginTop={50}>
            <View style={estilos.col}></View>
            <View style={estilos.col}></View>
          </View>

          <View style={estilos.row} marginTop={50}>
            <View style={estilos.col}>
              <Button
                style={{ alignSelf: "center", backgroundColor: "#a4161a" }}
                onPress={() => {
                  props.navigation.navigate("ContactoEmergencia");
                }}
              >
                <Text> Cancelar </Text>
              </Button>
            </View>
            <View style={estilos.col}>
              <Button
                style={{ alignSelf: "center", backgroundColor: "#52b69a" }}
                onPress={validaDatos}
              >
                <Text> Agregar </Text>
              </Button>
            </View>
          </View>
        </Form>
      </Content>
    </Container>
  );
};

export default ContactoEmergenciaAgregar;
