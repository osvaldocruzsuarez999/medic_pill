import { useFocusEffect } from '@react-navigation/core';
import React from 'react';
import {
  Container,
  Content,
  View,
  ListItem,
  Fab,
  List,
  Left,
  Body,
  Thumbnail,
  Text,
} from 'native-base';
import { Entypo } from '@expo/vector-icons';
const ContactoEmergencia = (props) => {
  /*
    Efecto qu actualice el título del Stack Navigator  
    desde los componentes Drawer Hijos
    Esto se tiene que actualizar cada vez aue cambiamos de Drawer.Screnn
     */
  useFocusEffect(() => {
    //Modificamos las opciones del HEader del Stack (padre)
    props.navigation.dangerouslyGetParent().setOptions({
      title: 'Contacto de emergencia',
    });
  });

  return (
    <Container>
      <Content>
        <List>
          <ListItem thumbnail>
            <Left>
              <Thumbnail
                square
                source={require('../../../assets/images/perdonar.jpg')}
              />
            </Left>
            <Body>
              <Text>Graciela Bolaños De Paz</Text>
              <Text note>Madre . .</Text>
            </Body>
          </ListItem>
        </List>
      </Content>

      <Fab
        containerStyle={{}}
        style={{ backgroundColor: '#52b69a' }}
        position='bottomRight'
        onPress={() => {
          props.navigation.navigate('ContactoEmergenciaAgregar');
        }}
      >
        <Entypo name='plus' />
      </Fab>
    </Container>
  );
};

export default ContactoEmergencia;
