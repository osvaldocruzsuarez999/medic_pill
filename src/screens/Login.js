import React, { useState } from "react";
import {
  Container,
  Content,
  Item,
  Input,
  H1,
  Text,
  Thumbnail,
  Button,
  View,
  Label,
} from "native-base";
import { ActivityIndicator, Alert } from "react-native";
import estilos from "../styles/estilos";
import firebase from "./../database/firebase";

const Login = (props) => {
  const [usuario, setUsuario] = useState("2017313039@uteq.edu.mx");
  const [pass, setPassword] = useState("12345678");
  /**
   * States para mostrar/ociultar spinner
   */
  const [btnVisible, setBtnVisible] = useState(true);
  const [aiVisible, setAiVisible] = useState(false);

  /**
   * State para habilitar/deshabilitar textInput's
   */
  const [tiHab, setTiHab] = useState(true);

  /**
   * Funcion que valida el formulario
   */
  const validaLogin = async () => {
    //Validamos usuario
    if (usuario.length < 5) {
      Alert.alert(
        "ERROR",
        "El correo ingresado no cumple con el formato",
        [
          {
            text: "Corregir",
            onPress: () => {
              setUsuario("");
            },
          },
        ],
        { cancelable: false }
      );
      return;
    }

    if (pass.length == null || pass.length <= 3) {
      Alert.alert(
        "ERROR",
        "Contraseña incorrecta",
        [
          {
            text: "Corregir",
            onPress: () => {
              setPassword("");
            },
          },
        ],
        { cancelable: false }
      );

      return;
    }

    /**
     * Si la validación es correcta llegamos
     * aqui
     */
    setAiVisible(true);
    setBtnVisible(false);
    setTiHab(false);

    try {
      const usuarioFirebase = await firebase.auth.signInWithEmailAndPassword(
        usuario,
        pass
      );
      let mensaje = `Bienvenido ${usuarioFirebase.user.email}\n`;
      mensaje += usuarioFirebase.user.emailVerified
        ? "Usuario verificado"
        : "Por favor valida tu cuenta";

      Alert.alert("Bienvenido", mensaje, [
        {
          text: "Aceptar",
          onPress: () => {
            setAiVisible(false);
            setBtnVisible(true);
            setTiHab(true);
            props.navigation.navigate("Home");
          },
        },
      ]);
    } catch (e) {
      Alert.alert("ERROR", get_error(e.code), [
        {
          text: "Corregir",
          onPress: () => {
            setAiVisible(false);
            setBtnVisible(true);
            setTiHab(true);
          },
        },
      ]);
    }
  };

  return (
    <Container>
      <Content padder>
        <Thumbnail
          source={require("./../../assets/images/login.png")}
          style={estilos.imgLogin}
        />

        <H1 style={{ textAlign: "center", color: "#1d3557" }}>
          Iniciar sesión
        </H1>

        <Item floatingLabel style={{ marginBottom: 20 }}>
          <Label>Usuario</Label>
          <Input
            keyboardType='email-address'
            style={estilos.input}
            onChangeText={(val) => setUsuario(val)}
            autoCapitalize='none'
            value={usuario}
            editable={tiHab}
          />
        </Item>

        <Item floatingLabel style={{ marginBottom: 10 }}>
          <Label>Contraseña</Label>
          <Input
            keyboardType='default'
            secureTextEntry
            style={estilos.input}
            onChangeText={(val) => setPassword(val)}
            value={pass}
            editable={tiHab}
          />
        </Item>

        <ActivityIndicator
          color='#1d3557'
          size='large'
          style={{
            display: aiVisible ? "flex" : "none",
            margin: 10,
          }}
        />

        <View
          style={{
            display: btnVisible ? "flex" : "none",
            alignSelf: "center",
            margin: 10,
          }}
        >
          <Button style={{ backgroundColor: "#457b9d" }} onPress={validaLogin}>
            <Text>Iniciar sesión</Text>
          </Button>
        </View>

        <Button
          style={{ alignSelf: "center" }}
          transparent
          onPress={() => {
            props.navigation.navigate("Registro");
          }}
        >
          <Text style={{ color: "#457b9d" }}>
            ¿No tienes una cuenta?, registrate aquí
          </Text>
        </Button>
      </Content>
    </Container>
  );
};

export default Login;
