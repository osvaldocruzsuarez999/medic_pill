import React, { useState } from "react";
import firebase from "./../database/firebase";
import { ActivityIndicator, Alert } from "react-native";
import {
  Container,
  Content,
  Item,
  Input,
  H1,
  DatePicker,
  Text,
  Thumbnail,
  Button,
  View,
  Label,
} from "native-base";
import estilos from "../styles/estilos";
import get_error from "../helpers/errores_es_mx";

const Registro = (props) => {
  /*
Creamos una funcion flecha anonima que permita 
crear un documento usuario en la colección usuarios
*/
  const [inputs, setInputs] = useState({
    nombres: "",
    apellido1: "",
    apellido2: "",
    fecha: "",
    telefono: "",
    email: "",
    pin: "",
  });

  /**
   * States para mostrar/ociultar spinner
   */
  const [btnVisible, setBtnVisible] = useState(true);
  const [aiVisible, setAiVisible] = useState(false);

  /**
   * State para habilitar/deshabilitar textInput's
   */
  const [tiHab, setTiHab] = useState(true);
  /**
   * State para habilitar/deshabilita Switch
   * */
  const [swtch, setSwtch] = useState(false);

  /**
   * State de Switch
   * */
  const [isEnabled, setIsEnabled] = useState(false);
  /**
   * State de para cambiar el estado de color toggleSwitch
   * */
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  /**
   * Funcion que valida el formulario
   */
  const validaDatos = async () => {
    //Validamos telefono (10 dígitos)
    if (inputs.telefono.length !== 10) {
      Alert.alert(
        "ERROR",
        "Teléfono incorrecto",
        [
          {
            text: "Corregir",
            onPress: () => {
              setInputs.telefono("");
            },
          },
        ],
        { cancelable: false }
      );
      return;
    }

    //Validamos pin

    //Validamos nombres
    if (inputs.nombres.length == null || inputs.nombres.length <= 3) {
      Alert.alert(
        "ERROR",
        "El nombre(s) es un campo obligatorio",
        [
          {
            text: "Agregar",
            onPress: () => {
              setInputs.nombres("");
            },
          },
        ],
        { cancelable: false }
      );
      return;
    }

    //Validamos apellido 1
    if (inputs.apellido1.length == null || inputs.apellido1.length <= 3) {
      Alert.alert(
        "ERROR",
        "El apellido es un campo obligatorio",
        [
          {
            text: "Agregar",
            onPress: () => {
              setInputs.apellido1("");
            },
          },
        ],
        { cancelable: false }
      );
      return;
    }

    //Validamos fecha nacimiento
    if (inputs.fecha.length !== 10) {
      console.log(inputs.fecha);
      let vregexNaix = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
      if (vregexNaix.test(inputs.fecha) == false) {
        Alert.alert(
          "ERROR",
          "La fecha es un campo obligatorio",
          [
            {
              text: "Agregar",
              onPress: () => {
                setInputs.fecha("");
              },
            },
          ],
          { cancelable: false }
        );
        return;
      }
    }

    /*/Validamos email
    if (inputs.email.length == 0 || inputs.email.length > 2) {
      let reg = /^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3})+$/;
      if (reg.test(inputs.email) == false) {
        Alert.alert(
          'ERROR',
          'Email no cumple con el formato',
          [
            {
              text: 'Corregir',
              onPress: () => {
                setInputs.email('');
              },
            },
          ],
          { cancelable: false }
        );
        return;
      }
    }
*/
    /**
     * Si la validación es correcta llegamos
     * aqui
     */
    setAiVisible(true);
    setBtnVisible(false);
    setTiHab(false);
    setSwtch(true);

    try {
      const usuarioFirebase = await firebase.auth.createUserWithEmailAndPassword(
        inputs.email,
        inputs.pin
      );

      const usuarioFS = await firebase.db.collection("usuarios").add(inputs);
      console.log(inputs);
      Alert.alert(
        "Usuario registrado",
        `ID insertado:\n\n${usuarioFS.id}\n\nINSERTAR DESDE EL FORMULARIO DE REGISTRO`,
        [{ text: "Pues ya que", onPress: null }],
        { cancelable: false }
      );
    } catch (e) {
      Alert.alert("ERROR", get_error(e.code), [
        {
          text: "Corregir",
          onPress: () => {
            this.setState({
              setAiVisible: false,
              setBtnVisible: true,
            });
          },
        },
      ]);
    }

    //Despues de 3 segundos, habilitar todo
    setTimeout(() => {
      setAiVisible(false);
      setBtnVisible(true);
      setTiHab(true);
      setSwtch(false);
    }, 3000);
  };

  return (
    //vista del app
    <Container>
      <Content padder>
        <Thumbnail
          source={require("./../../assets/images/registro.png")}
          style={estilos.imgRegistro}
        />
        <H1 style={{ textAlign: "center", color: "#1d3557", marginBottom: 20 }}>
          Registro
        </H1>

        <Text style={{ size: 10, marginBottom: 20 }}>* Campos requeridos </Text>

        <Item floatingLabel style={{ marginBottom: 20 }}>
          <Label>Nombre(s)*</Label>
          <Input
            keyboardType='default'
            maxLength={60}
            style={estilos.input}
            onChangeText={(value) => {
              setInputs({ ...inputs, nombres: value });
            }}
            value={inputs.nombres}
            editable={tiHab}
          />
        </Item>
        <View style={estilos.row}>
          <View style={estilos.col}>
            <Item floatingLabel style={{ marginBottom: 20 }}>
              <Label>Apellido paterno*</Label>
              <Input
                keyboardType='default'
                maxLength={23}
                style={estilos.input}
                onChangeText={(value) => {
                  setInputs({ ...inputs, apellido1: value });
                }}
                value={inputs.apellido1}
                editable={tiHab}
              />
            </Item>
          </View>
          <View style={estilos.col}>
            <Item floatingLabel style={{ marginBottom: 20 }}>
              <Label>Apellido materno</Label>
              <Input
                keyboardType='default'
                maxLength={23}
                style={estilos.input}
                onChangeText={(value) => {
                  setInputs({ ...inputs, apellido2: value });
                }}
                value={inputs.apellido2}
                editable={tiHab}
              />
            </Item>
          </View>
        </View>

        <View style={estilos.row}>
          <View style={estilos.col}>
            <Item floatingLabel style={{ marginBottom: 20 }}>
              <Label>Fecha de nacimiento* (dd/mm/aaaa)</Label>
              <Input
                keyboardType='default'
                maxLength={23}
                style={estilos.input}
                onChangeText={(value) => {
                  setInputs({ ...inputs, fecha: value });
                }}
                value={inputs.fecha}
                editable={tiHab}
              />
            </Item>
          </View>
          <View style={estilos.col}>
            <Item floatingLabel style={{ marginBottom: 20 }}>
              <Label>Teléfono (10 dígitos)*</Label>
              <Input
                keyboardType='phone-pad'
                maxLength={10}
                style={estilos.input}
                onChangeText={(value) => {
                  setInputs({ ...inputs, telefono: value });
                }}
                value={inputs.telefono}
                editable={tiHab}
              />
            </Item>
          </View>
        </View>
        <Item floatingLabel style={{ marginBottom: 20 }}>
          <Label>Email*</Label>
          <Input
            keyboardType='email-address'
            maxLength={55}
            style={estilos.input}
            onChangeText={(value) => {
              setInputs({ ...inputs, email: value });
            }}
            value={inputs.email}
            autoCapitalize='none'
            editable={tiHab}
          />
        </Item>

        <Item floatingLabel style={{ marginBottom: 20 }}>
          <Label>Contraseña*</Label>
          <Input
            keyboardType='default'
            secureTextEntry
            maxLength={25}
            style={estilos.input}
            onChangeText={(value) => {
              setInputs({ ...inputs, pin: value });
            }}
            value={inputs.pin}
            editable={tiHab}
          />
        </Item>

        <ActivityIndicator
          color='#1d3557'
          size='large'
          style={{ display: aiVisible ? "flex" : "none" }}
        />

        <View
          style={{
            margin: 15,
            display: btnVisible ? "flex" : "none",
            alignSelf: "center",
          }}
        >
          <Button style={{ backgroundColor: "#457b9d" }} onPress={validaDatos}>
            <Text>Registrarse</Text>
          </Button>
        </View>
        <Button
          style={{ alignSelf: "center" }}
          transparent
          onPress={() => {
            props.navigation.navigate("Login");
          }}
        >
          <Text style={{ color: "#457b9d" }}>
            ¿Ya tienes una cuenta?, inicia sesión aquí
          </Text>
        </Button>
      </Content>
    </Container>
  );
};

export default Registro;
