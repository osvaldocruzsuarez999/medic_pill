/**Cambio para hacer prueba  */
import React, { useEffect, useState } from "react";
import * as Font from "expo-font";
import { Ionicons } from "@expo/vector-icons";
import Login from "./src/screens/Login";
import Registro from "./src/screens/Registro";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Home from "./src/screens/private/Home";
import { ActivityIndicator, SafeAreaView } from "react-native";
import { Root, View } from "native-base";
import { LogBox } from "react-native";
/*prueba de cambio en git*/
const Stack = createStackNavigator();

export default function App() {

  useEffect(() => {
  
    LogBox.ignoreLogs([
      "Animated: `useNativeDriver`",
      "Setting a timer for a long period of time",
    ]);
  }, []);

  [isReady, setReady] = useState(false);
  useEffect(() => {
    native_base();
  });

  const app = !isReady ? (
    <SafeAreaView
      style={{
        justifyContent: "center",
        flex: 1,
      }}
    >
      <ActivityIndicator size='large' color='#000' />
    </SafeAreaView>
  ) : (
    <Root>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Login'>
          <Stack.Screen
            name='Login'
            options={{
              title: "Login",
              headerStyle: {
                backgroundColor: "#1a759E",
              },
              headerTintColor: "#fff",
            }}
            component={Login}
          />
          <Stack.Screen
            name='Registro'
            options={{
              title: "Registro",
              headerStyle: {
                backgroundColor: "#1a759E",
              },
              headerTintColor: "#fff",
            }}
            component={Registro}
          />

          <Stack.Screen
            name='Home'
            options={{
              title: "Home",
              headerStyle: {
                backgroundColor: "#1a759E",
              },
              headerTintColor: "#fff",
            }}
            component={Home}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Root>
  );
  return app;
}

async function native_base() {
  await Font.loadAsync({
    Roboto: require("native-base/Fonts/Roboto.ttf"),
    Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
    ...Ionicons.font,
  }).then(() => {
    setReady(true);
  });
}
